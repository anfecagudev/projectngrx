import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { RecipeService } from "../recipes/recipe.service";


import { Store } from "@ngrx/store";
import * as fromApp from '../store/app.reducer';
import * as RecipeActions from '../recipes/store/recipe.actions';
import { Recipe } from "../recipes/recipe.model";
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  url = 'https://recipebook-de829-default-rtdb.firebaseio.com/recipes.json';

  constructor(private http: HttpClient,
    private recipesService: RecipeService,
    private store: Store<fromApp.AppState>){}

  storeRecipes(){
    const recipes = this.recipesService.getRecipes();
    this.http.put(this.url, recipes)
    .subscribe(response => console.log(response));
  }

  fetchRecipes(){
    return this.http.get<Recipe[]>(this.url)
      .pipe(
      map(recipes => {
        return recipes.map(recipe => {
          return {
            ...recipe,
            ingredients: recipe.ingredients ? recipe.ingredients : []
          };
        });
      }),
      tap(recipes => {
        this.store.dispatch(new RecipeActions.SetRecipes(recipes));
      }));
  }
}
