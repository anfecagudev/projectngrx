import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, map, withLatestFrom, tap } from 'rxjs/operators';
import { Recipe } from '../recipe.model';
import * as RecipeActions from './recipe.actions';
import * as fromApp from '../../store/app.reducer';

@Injectable()
export class RecipeEffects {
  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) {}

  url = 'https://recipebook-de829-default-rtdb.firebaseio.com/recipes.json';


  saveRecipes = createEffect (() => this.actions$.pipe(
    ofType(RecipeActions.STORE_RECIPES),
    withLatestFrom(this.store.select('recipe')),
    switchMap(([actionData, recipesState]) => {
      return this.http
          .put(this.url, recipesState.recipes)
    })),
    { dispatch: false }
  );



    // withLatestFrom(this.store.select('recipe')),
    // tap(([actionData, recipesState]) => {
    //
    //     .subscribe((response) => console.log(response));
    // }),{dispatch: false}
  // )));

  fetchRecipes = createEffect(() =>
    this.actions$.pipe(
      ofType(RecipeActions.FETCH_RECIPES),
      switchMap(() => {
        return this.http.get<Recipe[]>(this.url).pipe(
          map((recipes) => {
            return recipes.map((recipe) => {
              return {
                ...recipe,
                ingredients: recipe.ingredients ? recipe.ingredients : [],
              };
            });
          }),
          map((recipes) => {
            return new RecipeActions.SetRecipes(recipes);
          })
        );
      })
    )
  );
}
